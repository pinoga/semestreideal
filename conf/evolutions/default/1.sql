# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table disciplina (
  id                        integer not null,
  sigla                     varchar(255),
  nome                      varchar(255),
  constraint pk_disciplina primary key (id))
;

create table usuario (
  id                        integer not null,
  username                  varchar(255),
  password                  varchar(255),
  constraint pk_usuario primary key (id))
;

create sequence disciplina_seq;

create sequence usuario_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists disciplina;

drop table if exists usuario;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists disciplina_seq;

drop sequence if exists usuario_seq;

