package controllers;

import play.*;
import play.mvc.*;
import play.db.ebean.*;
import com.avaje.ebean.Ebean;
import java.util.*;
import models.*;

import views.html.*;

import play.data.Form;


import models.Disciplina;
import models.Usuario;

public class Application extends Controller {
    
    public static Usuario usuarioLogado;
    
    public static Result index() {
        return ok(index.render());
    }    
    
    public static Result login() {
        return ok(
            login.render()
        );
    }
    
    public static Result verificaLogin() {
        Usuario usuario = Form.form(Usuario.class).bindFromRequest().get();
        if ("".equals(usuario.username)) return ok(senhaincorreta.render());
        List<Usuario> listaUsuarios = Usuario.find.all();
        for (Usuario contador : listaUsuarios) {
            if (usuario.username.equals(contador.username)) {
                    if (usuario.password.equals(contador.password)) {
                        usuarioLogado = usuario;
                        return redirect(routes.Application.index());
                    }
            }
        }
        return ok(senhaincorreta.render());
    }
    
    public static Result criaUsuario() {
        Usuario usuario = Form.form(Usuario.class).bindFromRequest().get();
        List<Usuario> listaUsuarios = Usuario.find.all();
        for (Usuario contador : listaUsuarios) {
            if (usuario.username.equals(contador.username)) {
                return ok(userexists.render());
            }
        }
        usuario.save();
        return redirect(routes.Application.login());
    }

    public static Result insereDisciplina() {
        //Disciplina disciplina = Form.form(Disciplina.class).bindFromRequest().get();
    	//usuarioLogado.disciplinas.add(disciplina);
       	return redirect(routes.Application.index());
    }
    
    public static Result mostraTodasDisciplinas() {
        List<Disciplina> todasDisciplinas = Disciplina.find.all();
        return mostraDisciplinas(todasDisciplinas);
    }
    
    public static Result mostraDisciplinasBuscadas() {
        Disciplina dummyDisciplina = Form.form(Disciplina.class).bindFromRequest().get();
        String fator = dummyDisciplina.nome;
        String disciplinasHtml = "";
        String htmlButton = 
        "<input type=\"submit\" value=\"Adicionar disciplina\">";
        List<Disciplina> lista = new ArrayList<Disciplina>();
        lista.addAll(Disciplina.procuraPorNome(fator));
        lista.addAll(Disciplina.procuraPorSigla(fator));
        for (Disciplina disciplina : lista) {
            disciplinasHtml +=  "<div>" + disciplina.sigla + " " + disciplina.nome 
                                /*+ htmlButton*/ + "</div>";
        }
        return ok(list.render(disciplinasHtml));        
    }
    
    public static Result mostraDisciplinasUsuario() {
        return redirect(routes.Application.index());
    }
    
    public static Result mostraDisciplinas(List<Disciplina> lista) {
        String disciplinasHtml = "";
        for (Disciplina disciplina : lista) {
            disciplinasHtml += "<h1>" + disciplina.sigla + " " + disciplina.nome + "</h1>";
        }
        return ok(list.render(disciplinasHtml));
    }
}
