package models;
import play.db.ebean.Model;
import javax.persistence.Id;
import javax.persistence.Entity;
import java.util.*;
import models.Disciplina;


@Entity
public class Usuario extends Model
{

	@Id
	public int id;
    
    public String username;
    public String password;
    
    public static Finder<String, Usuario> find = new Model.Finder<>(String.class, Usuario.class);
    
    public String getUsername() {
        return this.username;
    }
    public String getPassword() {
        return this.password;
    }
	public List<Disciplina> disciplinas = new ArrayList<Disciplina>();
	
}


