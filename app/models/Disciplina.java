

package models;
import play.db.ebean.Model;
import javax.persistence.Id;
import javax.persistence.Entity;
import java.util.*;



@Entity
public class Disciplina extends Model
{

	@Id
	public int id;

	public String sigla;
	
	public String nome;
	
	public static Finder<String, Disciplina> find = new Model.Finder<>(String.class, Disciplina.class);
	
	public static List<Disciplina> procuraPorNome(String nomebuscado) {
	    return find.where().like("nome", "%"+ nomebuscado +"%").findList();
	}
	public static List<Disciplina> procuraPorSigla(String siglabuscada) {
	    return find.where().like("sigla", "%"+ siglabuscada +"%").findList();
	}
}



